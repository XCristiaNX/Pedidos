var app = angular.module('app', []);

app.controller('controller', ["$scope", function ($scope) {
    db.collection("Products").onSnapshot(function (querySnapshot) {
        $scope.Products = [];
        querySnapshot.forEach(function (doc) {
            var data = doc.data();
            $scope.Products.push({
                Id: doc.id,
                Name: data.Name,
                ProductType: data.ProductType
            });
        });
        $scope.$apply();
    });

    $scope.SelectedFilters = [];
    db.collection("Filters").onSnapshot(function (querySnapshot) {
        $scope.Filters = [];
        querySnapshot.forEach(function (doc) {
            var data = doc.data();
            $scope.Filters.push({
                Id: doc.id,
                Name: data.Name,
                List: data.List
            });

            var indexArray = [];
            for (var i = 0; i < data.List.length; i++) {
                indexArray.push(0);
            }
            $scope.SelectedFilters.push({
                Name: data.Name,
                IndexArray: indexArray
            });
        });
        $scope.$apply();
    });

    $scope.SelectedProduct = function (model) {
        $scope.SelectedProductModel = model;
    }

    $scope.ShowFilterFunc = function (val) {
        $scope.ShowFilter = val;
    }

    $scope.ToggleActiveClass = function ($event, filterName, index) {
        for (var i = 0; i < $scope.SelectedFilters.length; i++) {
            if ($scope.SelectedFilters[i].Name == filterName) {
                $scope.SelectedFilters[i].IndexArray[index] = !$event.currentTarget.classList.contains("active") ? 1 : 0;
            }
        }

        if ($event.currentTarget.classList.contains("active")) {
            $event.currentTarget.classList.remove("active");
        }
        else {
            $event.currentTarget.classList.add("active");
        }
    };

    $scope.Search = function (value, index) {
        var arrayFiltros = [];
        for (var i = 0; i < $scope.SelectedFilters.length; i++) {
            if ($scope.SelectedFilters[i].IndexArray.indexOf(1) >= 0) {
                for (var y = 0; y < $scope.SelectedFilters[i].IndexArray.length; y++) {
                    if ($scope.SelectedFilters[i].IndexArray[y] == 1) {
                        switch ($scope.SelectedFilters[i].Name) {
                            case "Tipos de productos":
                                if (value.ProductType == y) {
                                    arrayFiltros.push(true);
                                }
                                else {
                                    arrayFiltros.push(false);
                                }
                                break;
                        }
                    }
                }
            }
        }

        if (arrayFiltros.length == 0) {
            return true;
        }

        for (var i = 0; i < arrayFiltros.length; i++) {
            if (arrayFiltros[i]) {
                return true;
            }
        }

        return false;
    };
}]);